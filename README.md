# POC HTTPs Server em Node JS 

## Sistema Operacional:

    MacOS

## Versão do Node: 

    node --version
    v13.7.0

## Versão do OpenSSL:

    openssl version 
    LibreSSL 2.8.3

## Gerar Certificado dentro da aplicação:

    openssl req -nodes -new -x509 -keyout certificado.key -out certificado.cert

### Instalar dependências:

    npm install

### Rodar aplicação:

    node index.js

## Testar API usando curl

### Versão TLS 1.0:

    curl --cacert certificado.cert --tlsv1.0 --tls-max 1.0 https://localhost:3000/ -v

ou

    curl --cacert certificado.cert --tlsv1 --tls-max 1.0 https://localhost:3000/ -v

Resposta:

    *   Trying ::1...
    * TCP_NODELAY set
    * Connected to localhost (::1) port 3000 (#0)
    * ALPN, offering h2
    * ALPN, offering http/1.1
    * successfully set certificate verify locations:
    *   CAfile: certificado.cert
    CApath: none
    * TLSv1.0 (OUT), TLS handshake, Client hello (1):
    * TLSv1.0 (IN), TLS alert, protocol version (582):
    * error:1400442E:SSL routines:CONNECT_CR_SRVR_HELLO:tlsv1 alert protocol version
    * Closing connection 0
    curl: (35) error:1400442E:SSL routines:CONNECT_CR_SRVR_HELLO:tlsv1 alert protocol version
    b

### Versão TLS 1.1:

    curl --cacert certificado.cert --tlsv1.1 --tls-max 1.1 https://localhost:3000/ -v

Resposta:

    *   Trying ::1...
    * TCP_NODELAY set
    * Connected to localhost (::1) port 3000 (#0)
    * ALPN, offering h2
    * ALPN, offering http/1.1
    * successfully set certificate verify locations:
    *   CAfile: certificado.cert
    CApath: none
    * TLSv1.1 (OUT), TLS handshake, Client hello (1):
    * TLSv1.1 (IN), TLS alert, protocol version (582):
    * error:1400442E:SSL routines:CONNECT_CR_SRVR_HELLO:tlsv1 alert protocol version
    * Closing connection 0
    curl: (35) error:1400442E:SSL routines:CONNECT_CR_SRVR_HELLO:tlsv1 alert protocol version

Versão TLS 1.2:

    curl --cacert certificado.cert --tlsv1.2 --tls-max 1.2 https://localhost:3000/ -v

Resposta:

    *   Trying ::1...
    * TCP_NODELAY set
    * Connected to localhost (::1) port 3000 (#0)
    * ALPN, offering h2
    * ALPN, offering http/1.1
    * successfully set certificate verify locations:
    *   CAfile: certificado.cert
    CApath: none
    * TLSv1.2 (OUT), TLS handshake, Client hello (1):
    * TLSv1.2 (IN), TLS handshake, Server hello (2):
    * TLSv1.2 (IN), TLS handshake, Certificate (11):
    * TLSv1.2 (IN), TLS handshake, Server key exchange (12):
    * TLSv1.2 (IN), TLS handshake, Server finished (14):
    * TLSv1.2 (OUT), TLS handshake, Client key exchange (16):
    * TLSv1.2 (OUT), TLS change cipher, Change cipher spec (1):
    * TLSv1.2 (OUT), TLS handshake, Finished (20):
    * TLSv1.2 (IN), TLS change cipher, Change cipher spec (1):
    * TLSv1.2 (IN), TLS handshake, Finished (20):
    * SSL connection using TLSv1.2 / ECDHE-RSA-AES256-GCM-SHA384
    * ALPN, server accepted to use http/1.1
    * Server certificate:
    *  subject: C=BR; ST=SP; L=Sao Paulo; O=Rede; OU=PN; CN=localhost; emailAddress=bruno@gmail.com
    *  start date: Mar 13 10:16:34 2021 GMT
    *  expire date: Apr 12 10:16:34 2021 GMT
    *  common name: localhost (matched)
    *  issuer: C=BR; ST=SP; L=Sao Paulo; O=Rede; OU=PN; CN=localhost; emailAddress=bruno@gmail.com
    *  SSL certificate verify ok.
    > GET / HTTP/1.1
    > Host: localhost:3000
    > User-Agent: curl/7.64.1
    > Accept: */*
    > 
    < HTTP/1.1 200 OK
    < X-Powered-By: Express
    < Content-Type: text/html; charset=utf-8
    < Content-Length: 24
    < ETag: W/"18-HGSC9dkaS1TsTMRUTJHz+OpfIOw"
    < Date: Sat, 13 Mar 2021 11:17:05 GMT
    < Connection: keep-alive
    < 
    * Connection #0 to host localhost left intact
    Hello world using HTTPS!
    * Closing connection 0

## Testar Serviço usando OpenSSL

### Versão TLS 1.0:

    openssl s_client -connect localhost:3000 -tls1  

Resposta:

    CONNECTED(00000005)
    4511035052:error:1400442E:SSL routines:CONNECT_CR_SRVR_HELLO:tlsv1 alert protocol version:/AppleInternal/BuildRoot/Library/Caches/com.apple.xbs/Sources/libressl/libressl-56.60.2/libressl-2.8/ssl/ssl_pkt.c:1200:SSL alert number 70
    4511035052:error:140040E5:SSL routines:CONNECT_CR_SRVR_HELLO:ssl handshake failure:/AppleInternal/BuildRoot/Library/Caches/com.apple.xbs/Sources/libressl/libressl-56.60.2/libressl-2.8/ssl/ssl_pkt.c:585:
    ---
    no peer certificate available
    ---
    No client certificate CA names sent
    ---
    SSL handshake has read 7 bytes and written 0 bytes
    ---
    New, (NONE), Cipher is (NONE)
    Secure Renegotiation IS NOT supported
    Compression: NONE
    Expansion: NONE
    No ALPN negotiated
    SSL-Session:
        Protocol  : TLSv1
        Cipher    : 0000
        Session-ID: 
        Session-ID-ctx: 
        Master-Key: 
        Start Time: 1615634336
        Timeout   : 7200 (sec)
        Verify return code: 0 (ok)
    ---

### Versão TLS 1.1:

    CONNECTED(00000005)
    4350353068:error:1400442E:SSL routines:CONNECT_CR_SRVR_HELLO:tlsv1 alert protocol version:/AppleInternal/BuildRoot/Library/Caches/com.apple.xbs/Sources/libressl/libressl-56.60.2/libressl-2.8/ssl/ssl_pkt.c:1200:SSL alert number 70
    4350353068:error:140040E5:SSL routines:CONNECT_CR_SRVR_HELLO:ssl handshake failure:/AppleInternal/BuildRoot/Library/Caches/com.apple.xbs/Sources/libressl/libressl-56.60.2/libressl-2.8/ssl/ssl_pkt.c:585:
    ---
    no peer certificate available
    ---
    No client certificate CA names sent
    ---
    SSL handshake has read 7 bytes and written 0 bytes
    ---
    New, (NONE), Cipher is (NONE)
    Secure Renegotiation IS NOT supported
    Compression: NONE
    Expansion: NONE
    No ALPN negotiated
    SSL-Session:
        Protocol  : TLSv1.1
        Cipher    : 0000
        Session-ID: 
        Session-ID-ctx: 
        Master-Key: 
        Start Time: 1615634383
        Timeout   : 7200 (sec)
        Verify return code: 0 (ok)
    ---

### Versão TLS 1.2:

    CONNECTED(00000005)
    depth=0 C = BR, ST = SP, L = Sao Paulo, O = Rede, OU = PN, CN = localhost, emailAddress = bruno@gmail.com
    verify error:num=18:self signed certificate
    verify return:1
    depth=0 C = BR, ST = SP, L = Sao Paulo, O = Rede, OU = PN, CN = localhost, emailAddress = bruno@gmail.com
    verify return:1
    ---
    Certificate chain
    0 s:/C=BR/ST=SP/L=Sao Paulo/O=Rede/OU=PN/CN=localhost/emailAddress=bruno@gmail.com
    i:/C=BR/ST=SP/L=Sao Paulo/O=Rede/OU=PN/CN=localhost/emailAddress=bruno@gmail.com
    ---
    Server certificate
    -----BEGIN CERTIFICATE-----
    MIIDeDCCAmACCQCX1x70VimSNTANBgkqhkiG9w0BAQsFADB+MQswCQYDVQQGEwJC
    UjELMAkGA1UECAwCU1AxEjAQBgNVBAcMCVNhbyBQYXVsbzENMAsGA1UECgwEUm ... CORTADO ...
    -----END CERTIFICATE-----
    subject=/C=BR/ST=SP/L=Sao Paulo/O=Rede/OU=PN/CN=localhost/emailAddress=bruno@gmail.com
    issuer=/C=BR/ST=SP/L=Sao Paulo/O=Rede/OU=PN/CN=localhost/emailAddress=bruno@gmail.com
    ---
    No client certificate CA names sent
    Server Temp Key: ECDH, X25519, 253 bits
    ---
    SSL handshake has read 1545 bytes and written 289 bytes
    ---
    New, TLSv1/SSLv3, Cipher is ECDHE-RSA-AES256-GCM-SHA384
    Server public key is 2048 bit
    Secure Renegotiation IS supported
    Compression: NONE
    Expansion: NONE
    No ALPN negotiated
    SSL-Session:
        Protocol  : TLSv1.2
        Cipher    : ECDHE-RSA-AES256-GCM-SHA384
        Session-ID: 18AB435EA5819B05D75BC405885BA93B7219E1227A2CBFC9E3A838E5A70F4E5C
        Session-ID-ctx: 
        Master-Key: 37436CE572811F25529C6409BF53DB1CA8B82C35EFBC781504223D7DC8 ... CORTADO ...
        TLS session ticket lifetime hint: 7200 (seconds)
        TLS session ticket:
        0000 - 8a 59 35 e8 e0 d9 6e b4-40 a5 7d 1f 19 aa a8 71   .Y5...n.@.}....q
         ... CORTADO ... 

        Start Time: 1615634432
        Timeout   : 7200 (sec)
        Verify return code: 18 (self signed certificate)
    ---
    read:errno=0


## Teste de validade dos Ciphers Suites:

### Testando sem a configuração de Cipher Suite:

Resposta:

    CONNECTED(00000005)
    depth=0 C = BR, ST = SP, L = Sao Paulo, O = Rede, OU = PN, CN = localhost, emailAddress = bruno@gmail.com
    verify error:num=18:self signed certificate
    verify return:1
    depth=0 C = BR, ST = SP, L = Sao Paulo, O = Rede, OU = PN, CN = localhost, emailAddress = bruno@gmail.com
    verify return:1
    ---
    Certificate chain
    0 s:/C=BR/ST=SP/L=Sao Paulo/O=Rede/OU=PN/CN=localhost/emailAddress=bruno@gmail.com
    i:/C=BR/ST=SP/L=Sao Paulo/O=Rede/OU=PN/CN=localhost/emailAddress=bruno@gmail.com
    ---
    Server certificate
    -----BEGIN CERTIFICATE-----
    MIIDeDCCAmACCQCX1x70VimSNTANBgkqhkiG9w0BAQsFADB+MQswCQYDVQQGEwJC
    UjELMAkGA1UECAwCU1AxEjAQBgNVBAcMCVNhbyBQYXVsbzENMAsGA1UECgwEUmVk
    ZTELMAkGA1UECwwCUE4xEjAQBgNVBAMMCWxvY2FsaG9zdDEeMBwGCSqGSIb3DQEJ
    ARYPYnJ1bm9AZ21haWwuY29tMB4XDTIxMDMxMzEwMTYzNFoXDTIxMDQxMjEwMTYz
    NFowfjELMAkGA1UEBhMCQlIxCzAJBgNVBAgMAlNQMRIwEAYDVQQHDAlTYW8gUGF1
    bG8xDTALBgNVBAoMBFJlZGUxCzAJBgNVBAsMAlBOMRIwEAYDVQQDDAlsb2NhbGhv
    c3QxHjAcBgkqhkiG9w0BCQEWD2JydW5vQGdtYWlsLmNvbTCCASIwDQYJKoZIhvcN
    AQEBBQADggEPADCCAQoCggEBAJV38WUPr0wDSnzq6qazftibCkWhBkCbA972m6Qd
    Niy23CyRYUi3hM8XIG1arjznT63n4wEreOqycqdZBcp5kYt7fTE/JrsbsGlOgbR5
    ... CORTADO ...
    -----END CERTIFICATE-----
    subject=/C=BR/ST=SP/L=Sao Paulo/O=Rede/OU=PN/CN=localhost/emailAddress=bruno@gmail.com
    issuer=/C=BR/ST=SP/L=Sao Paulo/O=Rede/OU=PN/CN=localhost/emailAddress=bruno@gmail.com
    ---
    No client certificate CA names sent
    Server Temp Key: ECDH, X25519, 253 bits
    ---
    SSL handshake has read 1545 bytes and written 289 bytes
    ---
    New, TLSv1/SSLv3, Cipher is ECDHE-RSA-AES128-GCM-SHA256
    Server public key is 2048 bit
    Secure Renegotiation IS supported
    Compression: NONE
    Expansion: NONE
    No ALPN negotiated
    SSL-Session:
        Protocol  : TLSv1.2
        Cipher    : ECDHE-RSA-AES128-GCM-SHA256
        Session-ID: B3FC24A03CA0D419C521A7CB041FCBD92FE399F11676894E65060F0A2A0AEB6B
        Session-ID-ctx: 
        Master-Key: 05FC534A2CB7A7E5E189B4B197AC2E21AFBBC65BD3008FC6F97046F49B ... CORTADO ...
        TLS session ticket lifetime hint: 7200 (seconds)
        TLS session ticket:
        0000 - a8 ff ec 5a f8 aa 78 86-e4 e9 55 be 52 94 f1 7b   ...Z..x...U.R..{
        0010 - 6a 69 35 64 f4 b6 82 69-a2 10 42 32 51 25 9b 5a   ji5d...i..B2Q%.Z
        0020 - 94 4a cd 14 fe e3 53 cf-ed f3 0c f8 61 47 94 2e   .J....S.....aG..
        0030 - e9 d3 22 38 36 78 55 58-91 f8 5f b2 a6 c2 9b 3d   . ... CORTADO ... 

        Start Time: 1615634732
        Timeout   : 7200 (sec)
        Verify return code: 18 (self signed certificate)
    ---
    read:errno=0

